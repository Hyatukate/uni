﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auswertung
{
    public class Point
    {
        public double x;
        public double y;
        public Point(double _x = 0f, double _y = 0f)
        {
            x = _x;
            y = _y;
        }       
    }

    class Program
    {
        static void Main(string[] args)
        {
            string[] fileNames = Directory.GetFiles(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\Data", "*.doc");
            float[] kelvin = new float[fileNames.Length];
            float[] pplus = new float[fileNames.Length];
            float[] dpplus = new float[fileNames.Length];
            float[] pminus = new float[fileNames.Length];
            float[] dpminus = new float[fileNames.Length];

            for (int f = 0; f < fileNames.Length; f++)
            {
                string[] tokens = fileNames[f].Split('\\');
                string fileName = tokens[tokens.Length - 1];
                string[] lines = File.ReadAllLines("Data\\" + fileName);

                double[] chan1 = new double[0]; ;
                double[] chan2 = new double[0]; ;

                bool isChan1 = false;

                double[] currentChan = new double[0];

                int subIndex = 0;

                for (int i = 0; i < lines.Length; i++)
                {
                    string currentLine = lines[i];
                    if (currentLine.StartsWith("#"))
                    {
                        if (currentLine.Contains("CHANNEL"))
                        {
                            isChan1 = int.Parse(currentLine.Split(':')[1].Substring(2)) == 1;
                        }
                        else if (currentLine.Contains("SIZE"))
                        {
                            int size = int.Parse(currentLine.Split('=')[1]);
                            currentChan = new double[size];
                            Console.WriteLine("Creating new Channel with size: " + size);
                        }
                    }
                    else if (currentLine == "")
                    {
                        if (isChan1)
                            chan1 = currentChan;
                        else
                            chan2 = currentChan;
                        subIndex = i + 1;
                    }
                    else
                    {
                        //Data
                        currentChan[i - subIndex] = float.Parse(currentLine, CultureInfo.InvariantCulture);
                    }
                }

                Console.WriteLine("Finished loading data points");


                double maxX = chan1.Max();
                maxX = maxX < 0.5f ? 0.5f : maxX;
                double minX = chan1.Min();
                minX = minX > -0.5f ? -0.5f : minX;
                double xRange = maxX - minX;

                double maxY = chan2.Max();
                double realMaxY = maxY;
                maxY = maxY < 0.5f ? 0.5f : maxY;
                double minY = chan2.Min();
                double realMinY = minY;
                minY = minY > -0.5f ? -0.5f : minY;
                double yRange = maxY - minY;

                int border = 16;
                int resolution = 1024;

                double xRation = (double)(resolution - 2 * border) / xRange;
                double yRation = (double)(resolution - 2 * border) / yRange;

                Bitmap graph = new Bitmap(resolution, resolution);
                short[,] dataGrid = new short[resolution, resolution];

                for (int i = 0; i < resolution; i++)
                {
                    for (int j = 0; j < resolution; j++)
                    {
                        if (i == resolution / 2 || j == resolution / 2)
                            graph.SetPixel(i, j, Color.Gray);
                        else
                            graph.SetPixel(i, j, Color.Black);
                    }
                }

                for (int i = 0; i < chan1.Length; i++)
                {
                    int x = (int)(xRation * (chan1[i] + Math.Abs(minX)) + 0.5f) + border;
                    int y = (resolution - border) - (int)(yRation * (chan2[i] + Math.Abs(minY)) + 0.5f);
                    graph.SetPixel(x, y, Color.Teal);
                    dataGrid[x, y] += 1;
                }
                
                graph.Save("Images/" + fileName.Split('.')[0] + "_1.png");

                int sqaureSize = resolution / 16;

                short[,] densityGrid = new short[dataGrid.GetLength(0) / sqaureSize, dataGrid.GetLength(1) / sqaureSize];
                short densityMax = 0;

                int densityX = densityGrid.GetLength(0);
                int densityY = densityGrid.GetLength(1);

                for (int i = 0; i < densityX; i++)
                {
                    for (int j = 0; j < densityY; j++)
                    {
                        short count = 0;
                        for (int k = 0; k < sqaureSize; k++)
                        {
                            for (int l = 0; l < sqaureSize; l++)
                            {
                                if (dataGrid[i * sqaureSize + k, j * sqaureSize + l] > 0)
                                    count += dataGrid[i * sqaureSize + k, j * sqaureSize + l];
                            }
                        }
                        densityGrid[i, j] = count;

                        if (count > densityMax)
                            densityMax = count;

                    }
                }

                List<Point> left = new List<Point>();
                List<Point> right = new List<Point>();

                for (int i = 0; i < densityX; i++)
                {
                    for (int j = 0; j < densityY; j++)
                    {
                        int r = 255 * densityGrid[i, j] / densityMax;

                        if (r > 25)
                        {
                            if (i < densityX / 2)
                                left.Add(new Point(i * sqaureSize + sqaureSize / 2, j * sqaureSize + sqaureSize / 2));
                            else
                                right.Add(new Point(i * sqaureSize + sqaureSize / 2, j * sqaureSize + sqaureSize / 2));
                        }

                        for (int k = 0; k < sqaureSize; k++)
                        {
                            for (int l = 0; l < sqaureSize; l++)
                            {
                                Color c = graph.GetPixel(i * sqaureSize + k, j * sqaureSize + l);

                                graph.SetPixel(i * sqaureSize + k, j * sqaureSize + l, Color.FromArgb((r + c.R) / 2, c.G, c.B));
                            }
                        }
                    }
                }

                graph.Save("Images/" + fileName.Split('.')[0] + "_2.png");

                if (f == 20)
                    Console.Write("Momment");

                List<Point> leftPoints = new List<Point>();
                List<Point> rightPoints = new List<Point>();

                for (int i = 0; i < chan1.Length; i++)
                {
                    int x = (int)(xRation * (chan1[i] + Math.Abs(minX)) + 0.5f) + border;
                    int y = (resolution - border) - (int)(yRation * (chan2[i] + Math.Abs(minY)) + 0.5f);

                    if (x < resolution / 2)
                    {
                        for (int j = 0; j < left.Count; j++)
                        {
                            double dx = x - left[j].x;
                            double dy = y - left[j].y;

                            double ration = chan2[i] / realMinY;

                            if (dx * dx + dy * dy <= (sqaureSize * sqaureSize) && ration > 0.875)
                                leftPoints.Add(new Point(chan1[i], chan2[i]));
                        }
                    }
                    else
                    {
                        for (int j = 0; j < right.Count; j++)
                        {
                            double dx = x - right[j].x;
                            double dy = y - right[j].y;

                            double ration = chan2[i] / realMaxY;

                            if (dx * dx + dy * dy <= (sqaureSize * sqaureSize) && ration > 0.875)
                                rightPoints.Add(new Point(chan1[i], chan2[i]));
                        }
                    }
                }

                for (int i = 0; i < leftPoints.Count; i++)
                {
                    int x = (int)(xRation * (leftPoints[i].x + Math.Abs(minX)) + 0.5f) + border;
                    int y = (resolution - border) - (int)(yRation * (leftPoints[i].y + Math.Abs(minY)) + 0.5f);
                    graph.SetPixel(x, y, Color.Red);
                }

                for (int i = 0; i < rightPoints.Count; i++)
                {
                    int x = (int)(xRation * (rightPoints[i].x + Math.Abs(minX)) + 0.5f) + border;
                    int y = (resolution - border) - (int)(yRation * (rightPoints[i].y + Math.Abs(minY)) + 0.5f);
                    graph.SetPixel(x, y, Color.Green);
                }

                graph.Save("Images/" + fileName.Split('.')[0] + "_3.png");


                double sum = 0;
                foreach (Point p in leftPoints)
                    sum += p.x;
                double leftMeanX = sum / leftPoints.Count;
                sum = 0;
                foreach (Point p in leftPoints)
                    sum += p.y;
                double leftMeanY = sum / leftPoints.Count;
                sum = 0;
                double sum2 = 0;
                foreach (Point p in leftPoints)
                {
                    sum += (p.x - leftMeanX) * (p.y - leftMeanY);
                    sum2 += (p.x - leftMeanX) * (p.x - leftMeanX);
                }
                double leftA = sum / (sum2 + 0.0001);
                double leftB = leftMeanY - leftA * leftMeanX;
                sum = 0;
                foreach (Point p in leftPoints)
                    sum += (leftA * p.x + leftB - p.y) * (leftA * p.x + leftB - p.y);
                sum /= (leftPoints.Count - 2 + 0.0001);
                double leftSigmaY = Math.Sqrt(sum);
                sum = 0;
                foreach (Point p in leftPoints)
                    sum += (p.x - leftMeanX) * (p.x - leftMeanX);
                sum = 1.0 / (sum + 0.0001);
                double leftSigmaA = leftSigmaY * Math.Sqrt(sum);
                sum = 0;
                sum2 = 0;
                foreach (Point p in leftPoints)
                {
                    sum += p.x * p.x;
                    sum2 += (p.x - leftMeanX) * (p.x - leftMeanX);
                }
                sum2 *= leftPoints.Count;
                sum /= sum2;
                double leftSigmaB = leftSigmaY * Math.Sqrt(sum);

                //RIGHT

                sum = 0;
                foreach (Point p in rightPoints)
                    sum += p.x;
                double rightMeanX = sum / rightPoints.Count;
                sum = 0;
                foreach (Point p in rightPoints)
                    sum += p.y;
                double rightMeanY = sum / rightPoints.Count;
                sum = 0;
                sum2 = 0;
                foreach (Point p in rightPoints)
                {
                    sum += (p.x - rightMeanX) * (p.y - rightMeanY);
                    sum2 += (p.x - rightMeanX) * (p.x - rightMeanX);
                }
                double rightA = sum / (sum2 + 0.0001);
                double rightB = rightMeanY - rightA * rightMeanX;
                sum = 0;
                foreach (Point p in rightPoints)
                    sum += (rightA * p.x + rightB - p.y) * (rightA * p.x + rightB - p.y);
                sum /= (rightPoints.Count - 2 + 0.0001);
                double rightSigmaY = Math.Sqrt(sum);
                sum = 0;
                foreach (Point p in rightPoints)
                    sum += (p.x - rightMeanX) * (p.x - rightMeanX);
                sum = 1.0 / (sum + 0.0001);
                double rightSigmaA = rightSigmaY * Math.Sqrt(sum);
                sum = 0;
                sum2 = 0;
                foreach (Point p in rightPoints)
                {
                    sum += p.x * p.x;
                    sum2 += (p.x - rightMeanX) * (p.x - rightMeanX);
                }
                sum2 *= rightPoints.Count;
                sum /= (sum2 + 0.0001);
                double rightSigmaB = rightSigmaY * Math.Sqrt(sum);

                if (leftPoints.Count > 0 && rightPoints.Count > 0)
                {

                    for (double i = minX; i < maxX; i += 0.01)
                    {
                        int x = (int)(xRation * (i + Math.Abs(minX)) + 0.5f) + border;
                        int y = (resolution - border) - (int)(yRation * (i * leftA + leftB + Math.Abs(minY)) + 0.5f);
                        graph.SetPixel(x, y, Color.Yellow);
                        y = (resolution - border) - (int)(yRation * (i * rightA + rightB + Math.Abs(minY)) + 0.5f);
                        graph.SetPixel(x, y, Color.Violet);
                    }

                    graph.Save("Images/" + fileName.Split('.')[0] + "_4.png");


                    Console.WriteLine("[Left] y = (" + leftA + " +- " + leftSigmaA + ")*x + (" + leftB + " +- " + leftSigmaB + ")");
                    Console.WriteLine("[Right] y = (" + rightA + " +- " + rightSigmaA + ")*x + (" + rightB + " +- " + rightSigmaB + ")");
                    
                    fileName = fileName.Split('.')[0].Replace('_', ',');
                    kelvin[f] = float.Parse(fileName);
                    pplus[f] = (float)rightB;
                    dpplus[f] = (float)rightSigmaB;
                    pminus[f] = (float)leftB;
                    dpminus[f] = (float)leftSigmaB;
                }
            }
            using (StreamWriter writer = new StreamWriter("rawTable.txt"))
            {
                for (int f = 0; f < kelvin.Length; f++)
                {
                    writer.WriteLine(kelvin[f] + "\t" + pplus[f] + "\t" + dpplus[f] + "\t" + pminus[f] + "\t" + dpminus[f]);
                }
            }
            using (StreamWriter writer = new StreamWriter("LaTeXTable.txt"))
            {
                writer.WriteLine("\\begin{table}[!ht]");
                writer.WriteLine("\\centering");
                writer.WriteLine("\\begin{tabular}{|l|l|l|} \\hline");
                writer.WriteLine("Temperatur in [K] & $P^+$ in [V] & $P^-$ in [V] \\\\\\hline \\hline");
                for (int f = 0; f < kelvin.Length; f++)
                {
                    writer.WriteLine(kelvin[f] + " & " + pplus[f] + " $\\pm$ " + dpplus[f] + " & " + pminus[f] + " $\\pm$ " + dpminus[f] + " \\\\\\hline");
                }
                writer.WriteLine("\\end{tabular}");
                writer.WriteLine("\\caption{Put Caption here}");
                writer.WriteLine("\\label{lable this bad boy}");
                writer.WriteLine("\\end{table}");
            }
        }
    }
}
